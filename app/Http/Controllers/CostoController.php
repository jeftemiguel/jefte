<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\costo;
use App\DetalleCosto;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use DB;


class CostoController extends Controller
{

    public function index(Request $request){
      
        if($request){
        
            $sql=trim($request->get('buscarTexto'));
           $costos=costo::join('proveedores','costos.idproveedor','=','proveedores.id')
            ->join('users','costos.idusuario','=','users.id')
            ->join('detalle_costos','costos.id','=','detalle_costos.idcosto')
             ->select('costos.id','costos.tipo_identificacion',
             'costos.num_costo','costos.fecha_costo','costos.impuesto',
             'costos.estado','costos.total','proveedores.nombre as proveedor','users.nombre')
            ->where('costos.num_costo','LIKE','%'.$sql.'%')
            ->orderBy('costos.id','desc')
            ->groupBy('costos.id','costos.tipo_identificacion',
            'costos.num_costo','costos.fecha_costo','costos.impuesto',
            'costos.estado','costos.total','proveedores.nombre','users.nombre')
            ->paginate(8);
             
 
            return view('costo.index',["costos"=>$costos,"buscarTexto"=>$sql]);
            
            //return $costos;
        }
      
 
     }
 
        public function create(){
 
             /*listar las proveedores en ventana modal*/
             $proveedores=DB::table('proveedores')->get();
            
             /*listar los productos en ventana modal*/
             $productos=DB::table('productos as prod')
             ->select(DB::raw('CONCAT(prod.codigo," ",prod.nombre) AS producto'),'prod.id')
             ->where('prod.condicion','=','1')->get(); 
 
             return view('costo.create',["proveedores"=>$proveedores,"productos"=>$productos]);
  
        }
 
         public function store(Request $request){
         
         //dd($request->all());
 
             try{
 
                 DB::beginTransaction();
 
                 $mytime= Carbon::now('America/Costa_Rica');
 
                 $costo = new costo();
                 $costo->idproveedor = $request->id_proveedor;
                 $costo->idusuario = \Auth::user()->id;
                 $costo->tipo_identificacion = $request->tipo_identificacion;
                 $costo->num_costo = $request->num_costo;
                 $costo->fecha_costo = $mytime->toDateString();
                 $costo->impuesto = '0.19';
                 $costo->total = $request->total_pagar;
                 $costo->estado = 'Registrado';
                 $costo->save();
 
                 $id_producto=$request->id_producto;
                 $cantidad=$request->cantidad;
                 $precio=$request->precio_costo;
                
 
                 
                 //Recorro todos los elementos
                 $cont=0;
     
                  while($cont < count($id_producto)){
 
                     $detalle = new DetalleCosto();
                     /*enviamos valores a las propiedades del objeto detalle*/
                     /*al idcosto del objeto detalle le envio el id del objeto costo, que es el objeto que se ingresó en la tabla costos de la bd*/
                     $detalle->idcosto = $costo->id;
                     $detalle->idproducto = $id_producto[$cont];
                     $detalle->cantidad = $cantidad[$cont];
                     $detalle->precio = $precio[$cont];    
                     $detalle->save();
                     $cont=$cont+1;
                 }
                     
                 DB::commit();
 
             } catch(Exception $e){
                 
                 DB::rollBack();
             }
 
             return Redirect::to('costo');
         }
 
         public function show($id){
 
             //dd($id);
            
             /*mostrar costo*/
 
             //$id = $request->id;
             $costo  = costo::join('proveedores','costos.idproveedor','=','proveedores.id')
             ->join('detalle_costos','costos.id','=','detalle_costos.idcosto')
             ->select('costos.id','costos.tipo_identificacion',
             'costos.num_costo','costos.fecha_costo','costos.impuesto',
             'costos.estado',DB::raw('sum(detalle_costos.cantidad*precio) as total'),'proveedores.nombre')
             ->where('costos.id','=',$id)
             ->orderBy('costos.id', 'desc')
             ->groupBy('costos.id','costos.tipo_identificacion',
             'costos.num_costo','costos.fecha_costo','costos.impuesto',
             'costos.estado','proveedores.nombre')
             ->first();
 
             /*mostrar detalles*/
             $detalles = DetalleCosto::join('productos','detalle_costos.idproducto','=','productos.id')
             ->select('detalle_costos.cantidad','detalle_costos.precio','productos.nombre as producto')
             ->where('detalle_costos.idcosto','=',$id)
             ->orderBy('detalle_costos.id', 'desc')->get();
             
             return view('costo.show',['costo' => $costo,'detalles' =>$detalles]);
         }
         
         public function destroy(Request $request){
 
     
                 $costo = costo::findOrFail($request->id_costo);
                 $costo->estado = 'Anulado';
                 $costo->save();
                 return Redirect::to('costo');
 
     }
 
         public function pdf(Request $request,$id){
         
             $costo = costo::join('proveedores','costos.idproveedor','=','proveedores.id')
             ->join('users','costos.idusuario','=','users.id')
             ->join('detalle_costos','costos.id','=','detalle_costos.idcosto')
             ->select('costos.id','costos.tipo_identificacion',
             'costos.num_costo','costos.created_at','costos.impuesto',DB::raw('sum(detalle_costos.cantidad*precio) as total'),
             'costos.estado','proveedores.nombre','proveedores.tipo_documento','proveedores.num_documento',
             'proveedores.direccion','proveedores.email','proveedores.telefono','users.usuario')
             ->where('costos.id','=',$id)
             ->orderBy('costos.id', 'desc')
             ->groupBy('costos.id','costos.tipo_identificacion',
             'costos.num_costo','costos.created_at','costos.impuesto',
             'costos.estado','proveedores.nombre','proveedores.tipo_documento','proveedores.num_documento',
             'proveedores.direccion','proveedores.email','proveedores.telefono','users.usuario')
             ->take(1)->get();
 
             $detalles = DetalleCosto::join('productos','detalle_costos.idproducto','=','productos.id')
             ->select('detalle_costos.cantidad','detalle_costos.precio',
             'productos.nombre as producto')
             ->where('detalle_costos.idcosto','=',$id)
             ->orderBy('detalle_costos.id', 'desc')->get();
 
             $numcosto=costo::select('num_costo')->where('id',$id)->get();
             
             $pdf= \PDF::loadView('pdf.costo',['costo'=>$costo,'detalles'=>$detalles]);
             return $pdf->download('costo-'.$numcosto[0]->num_costo.'.pdf');
         }
 
 
}