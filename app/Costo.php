<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costo extends Model
{
    //
    protected $table = 'costos';
        
        protected $fillable = [
            'idproveedor',
            'idusuario',
            'tipo_identificacion',
            'num_costo',
            'fecha_costo',
            'impuesto',
            'total',
            'estado'
        ];

        /*es el usuario que hace el registro*/
     public function usuario()
     {
         return $this->belongsTo('App\User');
     }

     /*el proveedor que hace la costo*/
     public function proveedor()
     {
         return $this->belongsTo('App\Proveedor');
     }

}