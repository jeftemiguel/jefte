<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCosto extends Model
{
    //
    protected $table = 'detalle_costos';
    protected $fillable = [
        'idcosto', 
        'idproducto',
        'cantidad',
        'precio'
          
    ];
    
    public $timestamps = false;
}