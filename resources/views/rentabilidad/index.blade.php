@extends('principal')
@section('contenido')
<main class="main">
<li class="breadcrumb-item active"><a href="/">SISTEMA DE GESTION RENTABILIDAD</a></li>
<div class="form-group row">
    <div class="col-md-6">
    <div class="input-group">
<input type="text" name="buscarTexto" class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
<button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>

</div>
<div class="card-body">
<td>
<div class="col-md-6">
                        <label class="form-control-label" for="cantidad">VALOR NETO</label>
                        
                        <input type="number" id="cantidad" name="cantidad" class="form-control" placeholder="Ingrese cantidad" pattern="[0-9]{0,15}">
                </div>
                <div class="col-md-6">
            <label class="form-control-label" for="precio_costo">C. PRODUCTO VENDIDO </label>
                        
                        <input type="number" id="precio_costo" name="precio_costo" class="form-control" placeholder="Ingrese productos vendidos" pattern="[0-9]{0,15}">
            </div>

</td>
</div>


</main>
@endsection